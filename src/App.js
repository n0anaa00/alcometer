
import './App.css';
import {useState} from 'react';

function App() {
  const [weight, setWeight] = useState(0);
  const [bottles, setBottles] = useState(0);
  const [time, setTime] = useState(0);
  const [result, setResult] = useState(0);
  const [gender, setGender] = useState(0);

  function calculate(e) {
    e.preventDefault();
    const calcLitres = 0.33 * bottles;
    const calcGrams = calcLitres * 8 * 4.5;
    const calcBurning = weight / 10;
    const calfGramsleft = calcGrams - (calcBurning * time)

    var male = document.getElementById("male");
    var female = document.getElementById("female");

    if(male.checked==true){
      setResult(calfGramsleft / (weight * 0.7));
    } else if(female.checked==true){
      setResult(calfGramsleft / (weight * 0.6));
    }

    
    
    
  }

  return (
    <form onSubmit={calculate}>
      <h3>Calculating Alcohol Blood Level</h3>
      
      <div>
        <label>Weight</label>
        <input type="number" step="1" 
        onChange={e => setWeight(e.target.value)}
        value={weight}/>
      </div>

      <div>
        <label>Bottles</label>
        
        <input type="number" step="1" 
        onChange={e => setBottles(e.target.value)}
        value={bottles}/>
      
      </div>

      <div>
        <label>Time</label>
        <input type="number" step="1" 
        onChange={e => setTime(e.target.value)}
        value={time}/>
      
      </div>
      

          <div>
            <label>Gender</label>
            <input type="radio" id="male" name="poll" onChange={e => setGender(e.target.value)}
            value={gender}/>
            <label for="male">Male</label>
            <input type="radio" id="female" name="poll" onChange={e => setGender(e.target.value)}
            value={gender}/>
            <label for="female">Female</label>
          </div>
      
      
      
     
      
      <div>
        <label>Result</label>
        <output>{result.toFixed(1)}</output> 
      
      </div>
      

      <div>
      <button>Calculate</button>

      </div>
    </form>
  );
}

export default App;
